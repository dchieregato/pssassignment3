/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package orders;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
/**
 *
 * @author david
 */
@Entity
public class Warehouse implements Serializable {
    private static final long serialVersionUID = 1L;
 
    // Persistent Fields:
    @Id @GeneratedValue
    int warehouseId;

    public int getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(int WarehouseId) {
        this.warehouseId = WarehouseId;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String City) {
        this.City = City;
    }
    private String City;
    public Warehouse(){
        
    }
    public Warehouse(String city){
        this.City=city;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + this.warehouseId;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Warehouse other = (Warehouse) obj;
        if (this.warehouseId != other.warehouseId) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return "Warehouse of "+City;
    }
}

package orders;
 
import java.io.Serializable;
import java.sql.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
 
@Entity
public class OrderClass implements Serializable {
    private static final long serialVersionUID = 1L;
 
    // Persistent Fields:
    @Id @GeneratedValue
    private int orderId;

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int id) {
        this.orderId = id;
    }
    private String name;
    private final Date orderDate;

    public Date getOrderDate() {
        return orderDate;
    }
    
    @ManyToOne(optional = true)
    private Warehouse magazzino;

    public Warehouse getMagazzino() {
        return magazzino;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + this.orderId;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final OrderClass other = (OrderClass) obj;
        if (this.orderId != other.orderId) {
            return false;
        }
        return true;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMagazzino(Warehouse magazzino) {
        this.magazzino = magazzino;
    }

    // Constructors:
    public OrderClass() {
        this.orderDate = new Date(System.currentTimeMillis());
    }
 
    public OrderClass(String name) {
        this.name = name;
        this.orderDate = new Date(System.currentTimeMillis());
    }
 
    // String Representation:
    @Override
    public String toString() {
        return orderId + " (order made on " + orderDate + ")";
    }
}
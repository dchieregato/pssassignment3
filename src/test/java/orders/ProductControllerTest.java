/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package orders;

import javax.faces.model.DataModel;
import javax.faces.model.SelectItem;
import orders.util.PaginationHelper;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author david
 */
public class ProductControllerTest {
    
    public ProductControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetSelected() {
        System.out.println("getSelected");
        ProductController instance = new ProductController();
        Product expResult = new Product();
        Product result = instance.getSelected();
        assertEquals(expResult, result);
        
    }

    @Test
    public void testPrepareList() {
        System.out.println("prepareList");
        ProductController instance = new ProductController();
        String expResult = "List";
        String result = instance.prepareList();
        assertEquals(expResult, result);
        
    }
    @Test
    public void testPrepareCreate() {
        System.out.println("prepareCreate");
        ProductController instance = new ProductController();
        String expResult = "Create";
        String result = instance.prepareCreate();
        assertEquals(expResult, result);
        
    }
}

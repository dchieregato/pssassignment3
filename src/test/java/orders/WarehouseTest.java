/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package orders;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author david
 */
public class WarehouseTest {
    
    public WarehouseTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetWarehouseId() {
        System.out.println("getWarehouseId");
        Warehouse instance = new Warehouse();
        int expResult = 0;
        int result = instance.getWarehouseId();
        assertEquals(expResult, result);
        
    }

    @Test
    public void testSetWarehouseId() {
        System.out.println("setWarehouseId");
        int WarehouseId = 0;
        Warehouse instance = new Warehouse();
        instance.setWarehouseId(WarehouseId);
        
    }

    @Test
    public void testGetCity() {
        System.out.println("getCity");
        Warehouse instance = new Warehouse();
        instance.setCity("Milano");
        String expResult = "Milano";
        String result = instance.getCity();
        assertEquals(expResult, result);
        
    }

    @Test
    public void testSetCity() {
        System.out.println("setCity");
        String City = "";
        Warehouse instance = new Warehouse();
        instance.setCity(City);
        
    }

    @Test
    public void testToString() {
        System.out.println("toString");
        Warehouse instance = new Warehouse();
        instance.setCity("Milano");
        String expResult = "Warehouse of Milano";
        String result = instance.toString();
        assertEquals(expResult, result);
        
    }
    
}

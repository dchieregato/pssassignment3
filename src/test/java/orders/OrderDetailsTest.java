/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package orders;

import java.util.Set;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author david
 */
public class OrderDetailsTest {
    
    public OrderDetailsTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetOrderProducts() {
        System.out.println("getOrderProducts");
        OrderDetails instance = new OrderDetails();
        Set<Product> expResult = null;
        Set<Product> result = instance.getOrderProducts();
        assertEquals(expResult, result);
        
    }

    @Test
    public void testSetOrderProducts() {
        System.out.println("setOrderProducts");
        Set<Product> orderProducts = null;
        OrderDetails instance = new OrderDetails();
        instance.setOrderProducts(orderProducts);
        
    }

    @Test
    public void testGetDeliveries() {
        System.out.println("getDeliveries");
        OrderDetails instance = new OrderDetails();
        Set<Delivery> expResult = null;
        Set<Delivery> result = instance.getDeliveries();
        assertEquals(expResult, result);
        
    }

    @Test
    public void testSetDeliveries() {
        System.out.println("setDeliveries");
        Set<Delivery> deliveries = null;
        OrderDetails instance = new OrderDetails();
        instance.setDeliveries(deliveries);
        
    }

    @Test
    public void testGetOrder() {
        System.out.println("getOrder");
        OrderDetails instance = new OrderDetails();
        Set<OrderClass> expResult = null;
        Set<OrderClass> result = instance.getOrder();
        assertEquals(expResult, result);
        
    }

    @Test
    public void testSetOrder() {
        System.out.println("setOrder");
        Set<OrderClass> order = null;
        OrderDetails instance = new OrderDetails();
        instance.setOrder(order);
        
    }

    @Test
    public void testGetId() {
        System.out.println("getId");
        OrderDetails instance = new OrderDetails();
        Long expResult = null;
        Long result = instance.getId();
        assertEquals(expResult, result);
        
    }

    @Test
    public void testSetId() {
        System.out.println("setId");
        Long id = null;
        OrderDetails instance = new OrderDetails();
        instance.setId(id);
        
    }

    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        OrderDetails instance = new OrderDetails();
        int expResult = 0;
        int result = instance.hashCode();
        assertEquals(expResult, result);
        
    }

    @Test
    public void testEquals() {
        System.out.println("equals");
        Object object = null;
        OrderDetails instance = new OrderDetails();
        boolean expResult = false;
        boolean result = instance.equals(object);
        assertEquals(expResult, result);
        
    }

    @Test
    public void testToString() {
        System.out.println("toString");
        OrderDetails instance = new OrderDetails();
        String expResult = "orders.OrderDetails[ id=null ]";
        String result = instance.toString();
        assertEquals(expResult, result);
        
    }
    
}

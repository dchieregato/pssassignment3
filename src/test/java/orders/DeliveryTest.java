/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package orders;

import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author david
 */
public class DeliveryTest {
    
    public DeliveryTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetDeliveryDate() {
        System.out.println("getDeliveryDate");
        Delivery instance = new Delivery();
        Date expResult = null;
        Date result = instance.getDeliveryDate();
        assertEquals(expResult, result);
        
    }

    @Test
    public void testSetDeliveryDate() {
        System.out.println("setDeliveryDate");
        Date deliveryDate = null;
        Delivery instance = new Delivery();
        instance.setDeliveryDate(deliveryDate);
        
    }

    @Test
    public void testGetTrackingNumber() {
        System.out.println("getTrackingNumber");
        Delivery instance = new Delivery();
        instance.setTrackingNumber("AA123456789");
        String expResult = "AA123456789";
        String result = instance.getTrackingNumber();
        assertEquals(expResult, result);
        
    }

    @Test
    public void testGetId() {
        System.out.println("getId");
        Delivery instance = new Delivery();
        Long expResult = null;
        Long result = instance.getId();
        assertEquals(expResult, result);
        
    }

    @Test
    public void testSetId() {
        System.out.println("setId");
        Long id = null;
        Delivery instance = new Delivery();
        instance.setId(id);
        
    }

    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        Delivery instance = new Delivery();
        int expResult = 0;
        int result = instance.hashCode();
        assertEquals(expResult, result);
        
    }

    @Test
    public void testEquals() {
        System.out.println("equals");
        Object object = null;
        Delivery instance = new Delivery();
        boolean expResult = false;
        boolean result = instance.equals(object);
        assertEquals(expResult, result);
        
    }

    @Test
    public void testToString() {
        System.out.println("toString");
        Delivery instance = new Delivery();
        String expResult = "orders.Delivery[ id=null ]";
        String result = instance.toString();
        assertEquals(expResult, result);
        
    }
    
}

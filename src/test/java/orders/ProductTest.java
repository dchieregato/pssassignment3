/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package orders;

import java.util.Set;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author david
 */
public class ProductTest {
    
    public ProductTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetRelatedProducts() {
        System.out.println("getRelatedProducts");
        Product instance = new Product();
        Set<Product> expResult = null;
        Set<Product> result = instance.getRelatedProducts();
        assertEquals(expResult, result);
        
    }

    @Test
    public void testSetRelatedProducts() {
        System.out.println("setRelatedProducts");
        Set<Product> relatedProducts = null;
        Product instance = new Product();
        instance.setRelatedProducts(relatedProducts);
        
    }

    @Test
    public void testGetProductDescription() {
        System.out.println("getProductDescription");
        Product instance = new Product();
        instance.setProductDescription("blablabla");
        String expResult = "blablabla";
        String result = instance.getProductDescription();
        assertEquals(expResult, result);
        
    }

    @Test
    public void testGetProductId() {
        System.out.println("getProductId");
        Product instance = new Product();
        int expResult = 0;
        int result = instance.getProductId();
        assertEquals(expResult, result);
        
    }

    @Test
    public void testSetProductId() {
        System.out.println("setProductId");
        int productId = 0;
        Product instance = new Product();
        instance.setProductId(productId);
        
    }

    @Test
    public void testEquals() {
        System.out.println("equals");
        Object obj = null;
        Product instance = new Product();
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
        
    }

    @Test
    public void testToString() {
        System.out.println("toString");
        Product instance = new Product();
        String expResult = "orders.Product[ id="+instance.getProductId()+" ]";
        String result = instance.toString();
        assertEquals(expResult, result);
        
    }
    
}

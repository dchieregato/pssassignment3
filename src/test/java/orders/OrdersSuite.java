/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package orders;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author david
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({orders.WarehouseTest.class, orders.OrderClassControllerTest.class, orders.WarehouseControllerTest.class, orders.ProductControllerTest.class, orders.ProductTest.class, orders.OrderDetailsControllerTest.class, orders.DeliveryTest.class, orders.OrderDetailsTest.class, orders.OrderClassTest.class, orders.DeliveryControllerTest.class})
public class OrdersSuite {

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }
    
}

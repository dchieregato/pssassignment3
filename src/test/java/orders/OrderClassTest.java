/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package orders;

import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author david
 */
public class OrderClassTest {
    
    public OrderClassTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetOrderId() {
        System.out.println("getOrderId");
        OrderClass instance = new OrderClass();
        int expResult = 0;
        int result = instance.getOrderId();
        assertEquals(expResult, result);
        
    }

    @Test
    public void testSetOrderId() {
        System.out.println("setOrderId");
        int id = 0;
        OrderClass instance = new OrderClass();
        instance.setOrderId(id);
        
    }

    @Test
    public void testGetOrderDate() {
        System.out.println("getOrderDate");
        OrderClass instance = new OrderClass();
        Date expResult = new Date();
        Date result = instance.getOrderDate();
        assertEquals(expResult, result);
        
    }

    @Test
    public void testGetMagazzino() {
        System.out.println("getMagazzino");
        OrderClass instance = new OrderClass();
        Warehouse expResult = null;
        Warehouse result = instance.getMagazzino();
        assertEquals(expResult, result);
        
    }

    @Test
    public void testSetName() {
        System.out.println("setName");
        String name = "";
        OrderClass instance = new OrderClass();
        instance.setName(name);
        
    }

    @Test
    public void testSetMagazzino() {
        System.out.println("setMagazzino");
        Warehouse magazzino = null;
        OrderClass instance = new OrderClass();
        instance.setMagazzino(magazzino);
        
    }

    @Test
    public void testToString() {
        System.out.println("toString");
        OrderClass instance = new OrderClass();
        instance.setName("abc");
        String expResult = instance.getOrderId()+" (order made on "+instance.getOrderDate().toString()+")";
        String result = instance.toString();
        assertEquals(expResult, result);
        
    }
    
}

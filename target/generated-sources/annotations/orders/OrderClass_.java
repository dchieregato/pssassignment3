package orders;

import java.sql.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import orders.Warehouse;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-01-03T00:49:42")
@StaticMetamodel(OrderClass.class)
public class OrderClass_ { 

    public static volatile SingularAttribute<OrderClass, Warehouse> magazzino;
    public static volatile SingularAttribute<OrderClass, Integer> orderId;
    public static volatile SingularAttribute<OrderClass, String> name;
    public static volatile SingularAttribute<OrderClass, Date> orderDate;

}
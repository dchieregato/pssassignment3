package orders;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import orders.Product;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-01-03T00:49:42")
@StaticMetamodel(Product.class)
public class Product_ { 

    public static volatile SingularAttribute<Product, Integer> productId;
    public static volatile SetAttribute<Product, Product> relatedProducts;
    public static volatile SingularAttribute<Product, String> productDescription;

}
David Chieregato - 806961

How to build this project:
This is a Maven project, it's made to tun in Netbeans because of ease of configuration.

- Clone this repository
- Open the Maven project in NetBeans:
Select File > Open Project....
Select the Assignment3 directory and click Open Project.
- Run the project:
Select Run > Run Main Project... (or F6).
Choose or define GlassFish as a server and click OK.
